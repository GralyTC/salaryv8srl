﻿function fn_callmethod(url, data, success, error, type) {
    $.ajax({
        type: type,
        url: url,
        data: data,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: success,
        error: error
    });
}

function fn_LoadTemplates(templateID, JsonObject) {
    var DateFormats = {
        short: "DD MMMM - YYYY",
        long: "dddd DD.MM.YYYY HH:mm"
    };

    Handlebars.registerHelper("formatDate", function (datetime, format) {
        if (moment) {
            format = DateFormats[format] || format;
            return moment(datetime).format(format);
        }
        else {
            return datetime;
        }
    });

    let stemplate = $("#" + templateID).html();
    let tmpl = Handlebars.compile(stemplate);
    let html = tmpl(JsonObject);
    return html;
}

function Fn_CreateDataTable(nameTable) {
    return $("#" + nameTable).DataTable({
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox editRow',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        order: [[4, 'asc']]
    });
}

function fn_message(type, message, divId) {
    let result = '';

    if (type === "s") {
        result = '<div><p class="alert alert-success">' + message + '</p></div>';
    } else if (type === "i") {
        result = '<div><p class="alert alert-info">' + message + '</p></div>';
    }
    else  {
        result = '<div><p class="alert alert-danger">' + message + '</p></div>';
    }

    $('div[id$=' + divId +']').empty().fadeIn().append(result);
    $('div[id$=' + divId + ']').delay("10000").fadeOut();
}

function fn_validateform(div) {
    return Fn_IsValidateForm("#" + div);
}

function Fn_IsValidateForm(objContainer = this.Designer.BoxColumnContent) {
    let isCorrect = true;
    try {
        const hashMap = new HashMap();
        Vanadium.each(Vanadium.all_elements_validators, (index, elementVanadium) => {
            $(objContainer).find(":input").each((index, elementHtml) => {
                if (elementVanadium.element && elementVanadium.element.id === elementHtml.id) {
                    elementVanadium.validations.forEach(elementVanadium => elementVanadium.param = elementVanadium.element);
                    hashMap.put(elementVanadium, elementVanadium.validate(elementHtml.value, elementHtml));
                }
            });
        });
        Vanadium.decorate(hashMap);
        $(objContainer).find(":input").each((index, elementHtml) =>
            $(elementHtml).hasClass('vanadium-invalid') && (index || $(elementHtml).focus())
        );
        $(objContainer).find(".vanadium-invalid").each(() => isCorrect = false);
    } catch (error) {
        console.error("FormControl.js/IsCorrectFormControlVanadium =>", error);
        isCorrect = false;
    }
    return isCorrect;
} 