﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalaryPresentation.Models.SalaryModels
{
    public class EmployeeReport
    {
        public string EmployeeCode { get; set; }
        public string EmployeeFullName { get; set; }
        public string DivisionName { get; set; }
        public string PositionName { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal TotalSalary { get; set; }
        public byte OfficeId { get; set; }
        public byte PositionId { get; set; }
        public int Grade { get; set; }
    }
}
