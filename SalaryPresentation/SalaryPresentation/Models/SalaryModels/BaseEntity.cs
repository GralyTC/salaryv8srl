﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalaryPresentation.Models.SalaryModels
{
    public class BaseEntity
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
