﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SalaryPresentation.Utilities.Entity;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SalaryPresentation.Utilities.Methods
{
    public class CallService
    {
        private static CallService instance = null;
        public static CallService Instance
        {
            get
            {
                if (instance == null)
                    instance = new CallService();
                return instance;
            }
        }

        public static string ServiceGetAll = "Salary/GetAll";
        public static string ServiceGetEmployeeSalaryByParamaters = "Salary/GetEmployeeSalaryByParamaters/{0}/{1}/{2}";
        public static string ServiceGetSalariesByEmployeeCode = "Salary/GetSalariesByEmployeeCode/{0}";
        public static string ServiceGetSaveSalaries = "Salary/SaveSalaries";

        public static string ServiceGetAllPosition = "Position/GetAll";
        public static string ServiceGetAllOffice = "Office/GetAll";
        public static string ServiceGetAllDivision = "Division/GetAll";


        public IConfigurationRoot configure;

        private IConfigurationRoot Configure() {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json").Build();
        }
        public async Task<ServiceResponse<T>> CallPost<T>(string endPoint, string data = "")
        {
            ServiceResponse<T> objResponse = new ServiceResponse<T>();
            string response = string.Empty;
            var config = Configure();
            string urlServices = config["URLService"] + endPoint;
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "")
                {
                    Content = new StringContent(data, Encoding.UTF8, "application/json")
                };

                using (HttpClient http = new HttpClient { BaseAddress = new Uri(urlServices) })
                {
                    HttpResponseMessage responseMessage = await http.SendAsync(request).ConfigureAwait(false);

                    if (!responseMessage.IsSuccessStatusCode)
                    {
                        throw new Exception(Convert.ToInt32(responseMessage.StatusCode).ToString() + "-" + responseMessage.StatusCode.ToString());
                    }

                    response = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                    objResponse = JsonConvert.DeserializeObject<ServiceResponse<T>>(response);
                }
            }
            catch (Exception ex)
            {

            }

            return objResponse;
        }

        public async Task<ServiceResponse<T>> CallGet<T>(string endPoint, string data = "")
        {
            ServiceResponse<T> objResponse = new ServiceResponse<T>();
            string response = string.Empty;
            var config = Configure();
            string urlServices = config["URLService"] + endPoint;
            try
            {
                string request = $"{urlServices}/{data}";

                using (HttpClient http = new HttpClient { BaseAddress = new Uri(urlServices) })
                {
                    HttpResponseMessage responseMessage = await http.GetAsync(request).ConfigureAwait(false);

                    if (!responseMessage.IsSuccessStatusCode)
                    {
                        throw new Exception(Convert.ToInt32(responseMessage.StatusCode).ToString() + "-" + responseMessage.StatusCode.ToString());
                    }

                    response = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                    objResponse = JsonConvert.DeserializeObject<ServiceResponse<T>>(response);
                }
            }
            catch (Exception ex)
            {

            }

            return objResponse;
        }
    }
}
