﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SalaryPresentation.Models;
using SalaryPresentation.Models.SalaryModels;
using SalaryPresentation.Utilities.Entity;
using SalaryPresentation.Utilities.Methods;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SalaryPresentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            var serviceResponseSalaries = await CallService.Instance.CallGet<EmployeeReport>(CallService.ServiceGetAll);
            var serviceResponsePosition = await CallService.Instance.CallGet<BaseEntity>(CallService.ServiceGetAllPosition);
            var serviceResponseOffice = await CallService.Instance.CallGet<BaseEntity>(CallService.ServiceGetAllOffice);

            ViewBag.ListSalary = serviceResponseSalaries.DataList;
            ViewBag.ListPosition = serviceResponsePosition.DataList;
            ViewBag.ListOffice = serviceResponseOffice.DataList;

            return View();
        }

        [HttpGet]
        [Route("GetEmployeeSalaryByParamaters")]
        public async Task<JsonResult> GetEmployeeSalaryByParamaters(string gradeName, int officeId, int positionId)
        {
            ServiceResponse<EmployeeReport> objResponse = null;
            try
            {
                string UrlService = string.Format(CallService.ServiceGetEmployeeSalaryByParamaters, gradeName, positionId.ToString(), officeId.ToString());
                objResponse = await CallService.Instance.CallGet<EmployeeReport>(UrlService);     
                if(objResponse.DataList == null)
                {
                    objResponse.Message = "Ocurrió un error interno.";
                }
            }
            catch (Exception)
            {
                objResponse.Message = "Ocurrió un error interno.";
            }
            return new JsonResult(objResponse);
        }

        [HttpGet]
        [Route("GetSalariesByEmployeeCode")]
        public async Task<JsonResult> GetSalariesByEmployeeCode(string employeeCode)
        {
            ServiceResponse<EmployeeReport> objResponse = null;
            try
            {
                string UrlService = string.Format(CallService.ServiceGetSalariesByEmployeeCode, employeeCode);
                objResponse = await CallService.Instance.CallGet<EmployeeReport>(UrlService);
            }
            catch (Exception)
            {
                objResponse.Message = "Ocurrió un error interno.";
            }
            return new JsonResult(objResponse);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        public async Task<IActionResult> Insert()
        {
            var serviceResponsePosition = await CallService.Instance.CallGet<BaseEntity>(CallService.ServiceGetAllPosition);
            var serviceResponseOffice = await CallService.Instance.CallGet<BaseEntity>(CallService.ServiceGetAllOffice);
            var serviceResponseDivision = await CallService.Instance.CallGet<BaseEntity>(CallService.ServiceGetAllDivision);

            ViewBag.ListPosition = serviceResponsePosition.DataList;
            ViewBag.ListOffice = serviceResponseOffice.DataList;
            ViewBag.ListDivision = serviceResponseDivision.DataList;

            return View();
        }

        [HttpPost]
        [Route("SaveSalaries")]
        public async Task<JsonResult> SaveSalaries(IList<Salary> salaryList)
        {
            ServiceResponse<bool> objResponse = null;
            try
            {
                objResponse = await CallService.Instance.CallPost<bool>(CallService.ServiceGetSaveSalaries, JsonConvert.SerializeObject(salaryList));
            }
            catch (Exception)
            {
                objResponse.Message = "Ocurrió un error interno.";
            }
            return new JsonResult(objResponse);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
