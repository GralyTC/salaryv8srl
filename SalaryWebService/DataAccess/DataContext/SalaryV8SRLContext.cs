﻿using System;
using Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace DataAccess.DataContext
{
    public partial class SalaryV8SRLContext : DbContext
    {
        public SalaryV8SRLContext()
        {
        }

        public SalaryV8SRLContext(DbContextOptions<SalaryV8SRLContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Division> Division { get; set; }
        public virtual DbSet<Office> Office { get; set; }
        public virtual DbSet<Position> Position { get; set; }
        public virtual DbSet<Salary> Salary { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Division>(entity =>
            {
                entity.Property(e => e.DivisionId).ValueGeneratedOnAdd();

                entity.Property(e => e.DivisionName)
                    .IsRequired()
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<Office>(entity =>
            {
                entity.Property(e => e.OfficeId).ValueGeneratedOnAdd();

                entity.Property(e => e.OfficeName)
                    .IsRequired()
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<Position>(entity =>
            {
                entity.Property(e => e.PositionId).ValueGeneratedOnAdd();

                entity.Property(e => e.PositionName)
                    .IsRequired()
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<Salary>(entity =>
            {
                entity.Property(e => e.BaseSalary).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.BeginDate).HasColumnType("date");

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.Commission).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CompensationBonus).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Contributions).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.EmployeeCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.EmployeeName)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.EmployeeSurname)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.IdentificationNumber)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.ProductionBonus).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.Division)
                    .WithMany(p => p.Salary)
                    .HasForeignKey(d => d.DivisionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DivisionId");

                entity.HasOne(d => d.Office)
                    .WithMany(p => p.Salary)
                    .HasForeignKey(d => d.OfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OfficeId");

                entity.HasOne(d => d.Position)
                    .WithMany(p => p.Salary)
                    .HasForeignKey(d => d.PositionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PositionId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
