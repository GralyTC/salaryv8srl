using BusinessLogic.Interfaces;
using BusinessLogic.Process;
using DataAccess.DataContext;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Repository.Interfaces;
using Repository.Repositories;
using System;

namespace SalaryService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<SalaryV8SRLContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("SalaryV8SRL"))
            );

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddTransient<ISalary, SalaryProcess>();
            services.AddTransient<IOffice, OfficeProcess>();
            services.AddTransient<IPosition, PositionProcess>();
            services.AddTransient<IDivision, DivisionProcess>();
            services.AddTransient<ISalaryRepository, SalaryRepository>();
            services.AddTransient<IDivisionRepository, DivisionRepository>();
            services.AddTransient<IOfficeRepository, OfficeRepository>();
            services.AddTransient<IPositionRepository, PositionRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
