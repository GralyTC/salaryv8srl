﻿using BusinessLogic.DTOs;
using BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Entity;

namespace SalaryService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalaryController : ControllerBase
    {
        private readonly ISalary _salaryProcess;

        public SalaryController(ISalary salaryProcess)
        {
            _salaryProcess = salaryProcess;
        }

        [HttpGet("GetAll")]
        public async Task<ServiceResponse<EmployeeReportDTO>> GetAll()
        {
            return await _salaryProcess.GetAll();
        }

        [Route("GetEmployeeSalaryByParamaters/{gradeId}/{positionId}/{officeId}")]
        [HttpGet]
        public async Task<ServiceResponse<EmployeeReportDTO>> GetEmployeeSalaryByParamaters(byte gradeId, byte positionId, byte officeId)
        {
            return await _salaryProcess.GetEmployeeSalaryByParamaters(gradeId, positionId, officeId);
        }

        [Route("GetSalariesByEmployeeCode/{employeeCode}")]
        [HttpGet]
        public async Task<ServiceResponse<EmployeeReportDTO>> GetSalariesByEmployeeCode(string employeeCode)
        {
            return await _salaryProcess.GetSalariesByEmployeeCode(employeeCode);
        }

        [HttpPost("SaveSalaries")]
        public async Task<ServiceResponse<bool>> SaveSalaries(IList<SalaryDTO> salaryList)
        {
            return await _salaryProcess.SaveSalaries(salaryList);
        }
    }
}
