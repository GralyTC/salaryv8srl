﻿using BusinessLogic.DTOs;
using BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Entity;

namespace SalaryService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DivisionController : ControllerBase
    {
        private readonly IDivision _divisionProcess;

        public DivisionController(IDivision divisionProcess)
        {
            _divisionProcess = divisionProcess;
        }

        [HttpGet("GetAll")]
        public async Task<ServiceResponse<BaseEntityDTO>> GetAll()
        {
            return await _divisionProcess.GetAll();
        }
    }
}
