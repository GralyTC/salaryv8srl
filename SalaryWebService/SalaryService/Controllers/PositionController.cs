﻿using BusinessLogic.DTOs;
using BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Utilities.Entity;

namespace SalaryService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : ControllerBase
    {
        private readonly IPosition _positionProcess;

        public PositionController(IPosition positionProcess)
        {
            _positionProcess = positionProcess;
        }

        [HttpGet("GetAll")]
        public async Task<ServiceResponse<BaseEntityDTO>> GetAll()
        {
            return await _positionProcess.GetAll();
        }
    }
}
