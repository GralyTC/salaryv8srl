﻿using System.Collections.Generic;

namespace Utilities.Entity
{
    public class ServiceResponse<T>
    {
        public bool Success { get; set; }
        public IList<T> DataList { get; set; }
        public string Message { get; set; }
        public string Custom { get; set; }
    }
}
