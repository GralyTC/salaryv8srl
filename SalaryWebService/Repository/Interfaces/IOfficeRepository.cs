﻿using Entity;

namespace Repository.Interfaces
{
    public interface IOfficeRepository : IBaseRepository<Office>
    {
    }
}
