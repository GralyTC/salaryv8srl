﻿using Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interfaces
{
    public interface IPositionRepository : IBaseRepository<Position>
    {
    }
}
