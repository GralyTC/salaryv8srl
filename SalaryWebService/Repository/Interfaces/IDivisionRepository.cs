﻿using Entity;

namespace Repository.Interfaces
{
    public interface IDivisionRepository : IBaseRepository<Division>
    {
    }
}
