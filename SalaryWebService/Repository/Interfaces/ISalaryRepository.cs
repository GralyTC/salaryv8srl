﻿using Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface ISalaryRepository : IBaseRepository<Salary>
    {
        Task<IList<Salary>> GetEmployeeSalaryByParamaters(byte gradeId, byte positionId, byte officeId);
        Task<IList<Salary>> GetSalariesByEmployeeCode(string employeeCode);
        Task<bool> SearchSalaryIntersect(IList<Salary> salaryList);

        Task SaveSalaries(IList<Salary> salaryList);
    }
}
