﻿using DataAccess.DataContext;
using Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace Repository.Repositories
{
    public class SalaryRepository : ISalaryRepository
    {
        private readonly SalaryV8SRLContext _salaryContext;

        public SalaryRepository(SalaryV8SRLContext salaryContext)
        {
            _salaryContext = salaryContext;
        }

        public async Task<IList<Salary>> GetAll()
        {
            var salaryListAll = await _salaryContext.Salary.Include(x => x.Division)
                                    .Include(x => x.Office)
                                    .Include(x => x.Position).ToListAsync();
            var salaryList = salaryListAll.GroupBy(s => s.EmployeeCode)
             .Select(s => s.OrderByDescending(x => x.Year).ThenByDescending(x => x.Month).FirstOrDefault()).ToList();
            return salaryList;
        }

        public async Task<IList<Salary>> GetEmployeeSalaryByParamaters(byte gradeId, byte positionId, byte officeId)
        {
            var salaryList = GetAll();
            var salaryListWithFilter = salaryList.Result.Where(x => 
                                                        (x.Grade == gradeId || gradeId == 0) && 
                                                        (x.PositionId == positionId || positionId == 0) && 
                                                        (x.OfficeId == officeId || officeId == 0) ).ToList();
            return salaryListWithFilter;
        }

        public async Task<IList<Salary>> GetSalariesByEmployeeCode(string employeeCode)
        {
            var salaryList = await _salaryContext.Salary.Where(x => x.EmployeeCode == employeeCode)
                                    .Include(x => x.Division)
                                    .Include(x => x.Office)
                                    .Include(x => x.Position)
                                    .ToListAsync();
            return salaryList;
        }

        public async Task<bool> SearchSalaryIntersect(IList<Salary> salaryList) {
            var data = await _salaryContext.Salary.ToListAsync();
            return data.Any(x => salaryList.Any(s => s.EmployeeCode == x.EmployeeCode && s.Year == x.Year && s.Month == x.Month));
        }

        public async Task SaveSalaries(IList<Salary> salaryList)
        {
            await _salaryContext.Salary.AddRangeAsync(salaryList);
            await _salaryContext.SaveChangesAsync();
        }
    }
}
