﻿using DataAccess.DataContext;
using Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class PositionRepository : IPositionRepository
    {
        private readonly SalaryV8SRLContext _salaryContext;

        public PositionRepository(SalaryV8SRLContext salaryContext)
        {
            _salaryContext = salaryContext;
        }

        public async Task<IList<Position>> GetAll()
        {
            var positionList = await _salaryContext.Position.ToListAsync();
            return positionList;
        }
    }
}

