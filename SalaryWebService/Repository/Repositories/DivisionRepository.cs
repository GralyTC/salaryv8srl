﻿using DataAccess.DataContext;
using Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class DivisionRepository : IDivisionRepository
    {
        private readonly SalaryV8SRLContext _salaryContext;

        public DivisionRepository(SalaryV8SRLContext salaryContext) {
            _salaryContext = salaryContext;
        }

        public async Task<IList<Division>> GetAll() {
            var divisionList = await _salaryContext.Division.ToListAsync();
            return divisionList;
        }
    }

}
