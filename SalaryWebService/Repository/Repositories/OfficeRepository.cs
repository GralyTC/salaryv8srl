﻿using DataAccess.DataContext;
using Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class OfficeRepository : IOfficeRepository
    {
        private readonly SalaryV8SRLContext _salaryContext;

        public OfficeRepository(SalaryV8SRLContext salaryContext)
        {
            _salaryContext = salaryContext;
        }

        public async Task<IList<Office>> GetAll()
        {
            var officeList = await _salaryContext.Office.ToListAsync();
            return officeList;
        }
    }
}
