﻿using System;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Entity
{
    public partial class Salary
    {
        public int SalaryId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public byte OfficeId { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSurname { get; set; }
        public byte DivisionId { get; set; }
        public byte PositionId { get; set; }
        public int Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commission { get; set; }
        public decimal Contributions { get; set; }

        public virtual Division Division { get; set; }
        public virtual Office Office { get; set; }
        public virtual Position Position { get; set; }
    }
}
