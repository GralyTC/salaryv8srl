﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Entity
{
    public partial class Office
    {
        public Office()
        {
            Salary = new HashSet<Salary>();
        }

        public byte OfficeId { get; set; }
        public string OfficeName { get; set; }

        public virtual ICollection<Salary> Salary { get; set; }
    }
}
