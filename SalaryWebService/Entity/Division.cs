﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Entity
{
    public partial class Division
    {
        public Division()
        {
            Salary = new HashSet<Salary>();
        }

        public byte DivisionId { get; set; }
        public string DivisionName { get; set; }

        public virtual ICollection<Salary> Salary { get; set; }
    }
}
