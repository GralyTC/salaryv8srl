﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Entity
{
    public partial class Position
    {
        public Position()
        {
            Salary = new HashSet<Salary>();
        }

        public byte PositionId { get; set; }
        public string PositionName { get; set; }

        public virtual ICollection<Salary> Salary { get; set; }
    }
}
