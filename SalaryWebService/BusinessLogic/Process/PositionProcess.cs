﻿using AutoMapper;
using BusinessLogic.DTOs;
using BusinessLogic.Interfaces;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilities.Entity;

namespace BusinessLogic.Process
{
    public class PositionProcess : IPosition
    {
        private readonly IPositionRepository _positionRepository;
        private readonly IMapper _mapper;

        public PositionProcess(IPositionRepository positionRepository, IMapper mapper)
        {
            _positionRepository = positionRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResponse<BaseEntityDTO>> GetAll()
        {
            ServiceResponse<BaseEntityDTO> serviceResponse = new ServiceResponse<BaseEntityDTO>();

            try
            {
                var positionList = await _positionRepository.GetAll();
                serviceResponse.DataList = _mapper.Map<IList<BaseEntityDTO>>(positionList);
                serviceResponse.Success = true;
            }
            catch (Exception)
            {

            }

            return serviceResponse;
        }
    }
}
