﻿using AutoMapper;
using BusinessLogic.DTOs;
using BusinessLogic.Interfaces;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Entity;

namespace BusinessLogic.Process
{
    public class DivisionProcess : IDivision
    {
        private readonly IDivisionRepository _divisionRepository;
        private readonly IMapper _mapper;

        public DivisionProcess(IDivisionRepository divisionRepository, IMapper mapper)
        {
            _divisionRepository = divisionRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResponse<BaseEntityDTO>> GetAll()
        {
            ServiceResponse<BaseEntityDTO> serviceResponse = new ServiceResponse<BaseEntityDTO>();

            try
            {
                var divisionList = await _divisionRepository.GetAll();
                serviceResponse.DataList = _mapper.Map<IList<BaseEntityDTO>>(divisionList);
                serviceResponse.Success = true;
            }
            catch (Exception)
            {

            }

            return serviceResponse;
        }
    }
}
