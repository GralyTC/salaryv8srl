﻿using AutoMapper;
using BusinessLogic.DTOs;
using BusinessLogic.Interfaces;
using Entity;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Entity;

namespace BusinessLogic.Process
{
    public class SalaryProcess : ISalary
    {
        private readonly ISalaryRepository _salaryRepository;
        private readonly IMapper _mapper;

        public SalaryProcess(ISalaryRepository salaryRepository, IMapper mapper)
        {
            _salaryRepository = salaryRepository;
            _mapper = mapper;
        }
        public async Task<ServiceResponse<EmployeeReportDTO>> GetAll()
        {

            ServiceResponse<EmployeeReportDTO> serviceResponse = new ServiceResponse<EmployeeReportDTO>();

            try
            {
                var salaryList = await _salaryRepository.GetAll();
                serviceResponse.DataList = _mapper.Map<IList<EmployeeReportDTO>>(CalculateSalary(salaryList.ToList()));
                serviceResponse.Success = true;
            }
            catch (Exception)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = "Ocurrió un error interno.";
            }

            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeReportDTO>> GetEmployeeSalaryByParamaters(byte gradeId, byte positionId, byte officeId)
        {
            ServiceResponse<EmployeeReportDTO> serviceResponse = new ServiceResponse<EmployeeReportDTO>();
            try
            {
                var salaryList = await _salaryRepository.GetEmployeeSalaryByParamaters(gradeId, positionId, officeId);
                serviceResponse.DataList = _mapper.Map<IList<EmployeeReportDTO>>(CalculateSalary(salaryList.ToList())); 
                serviceResponse.Success = true;
                if(serviceResponse.DataList.Count() == 0)
                {
                    serviceResponse.Message = "No existen registros.";
                }
            }
            catch (Exception)
            {
                serviceResponse.Message = "Ocurrió un error interno.";
            }
            return serviceResponse;
        }

        private IList<SalaryDTO> CalculateSalary(IList<Salary> salaryList) {
            var salariesDto = _mapper.Map<IList<SalaryDTO>>(salaryList);
            for (int i = 0; i < salaryList.Count; i++)
            {
                decimal otherIncome = (salaryList[i].BaseSalary + salaryList[i].Commission) * 0.08M + salaryList[i].Commission;
                decimal totalSalary = salaryList[i].BaseSalary + salaryList[i].ProductionBonus + (salaryList[i].CompensationBonus * 0.75M) + otherIncome - salaryList[i].Contributions;
                salariesDto[i].TotalSalary = totalSalary;
            }
            return salariesDto;
        }

        public async Task<ServiceResponse<EmployeeReportDTO>> GetSalariesByEmployeeCode(string employeeCode) {
            ServiceResponse<EmployeeReportDTO> serviceResponse = new ServiceResponse<EmployeeReportDTO>();
            decimal bonus = 0;
            int consecutive = 0;
            bool band = false;
            try
            {                
                var salaryListAll = await _salaryRepository.GetSalariesByEmployeeCode(employeeCode);
                if (salaryListAll.Count() > 0)
                {
                    var lastThreeSalaries = salaryListAll.OrderByDescending(x => x.Year).ThenByDescending(x => x.Month).Take(3);
                    var lastThreeSalariesCalculate = CalculateSalary(lastThreeSalaries.ToList());
                    var lastThreeSalariesDto = _mapper.Map<IList<SalaryDTO>>(lastThreeSalariesCalculate);
                    if (lastThreeSalariesDto.Count > 1)
                    {
                        for (int i = 1; i < lastThreeSalariesDto.Count(); i++)
                        {
                            // 0 : 2021-02
                            // 1 : 2021-01
                            // 2 : 2020-12
                            consecutive = Convert.ToInt32(lastThreeSalariesDto[i - 1].YearMonth) - Convert.ToInt32(lastThreeSalariesDto[i].YearMonth);
                            if ((consecutive != 1 && consecutive != 89) || band == true)
                            {
                                band = true;
                                lastThreeSalariesDto[i].SalaryId = 0;
                            }
                        }
                    }
                    lastThreeSalariesDto = lastThreeSalariesDto.Where(x => x.SalaryId != 0).ToList();
                    bonus = lastThreeSalariesDto.Sum(x => x.TotalSalary) / 3;
                    serviceResponse.Custom = bonus.ToString("F");
                    serviceResponse.DataList = _mapper.Map<IList<EmployeeReportDTO>>(lastThreeSalariesDto);
                    serviceResponse.Success = true;
                }
                else {
                    serviceResponse.Success = false;
                    serviceResponse.Message = "El empleado no tiene salarios registrados.";
                }
            }
            catch (Exception e)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = "Ocurrió un error interno.";
            }
            return serviceResponse;
        }

        public async Task<ServiceResponse<bool>> SaveSalaries(IList<SalaryDTO> salaryListDto)
        {
            ServiceResponse<bool> serviceResponse = new ServiceResponse<bool>();

            try
            {
                var repeatEmployees = salaryListDto.GroupBy(x => new { x.EmployeeCode, x.Month, x.Year }).Where(count => count.Count() > 1);
                if (repeatEmployees.Count() > 0)
                {
                    serviceResponse.Message = "No puede ingresar más de un registro salarial de un empleado para el mismo mes y año.";
                }
                else
                {
                    IList<Salary> salaryList = new List<Salary>();
                    foreach (var item in salaryListDto)
                    {
                        var salary = _mapper.Map<Salary>(item);
                        salaryList.Add(salary);
                    }
                    var repeatInDB = await _salaryRepository.SearchSalaryIntersect(salaryList);
                    if (repeatInDB)
                    {
                        serviceResponse.Message = "El empleado ya tiene un registro salarial para el mismo mes y año.";
                    }
                    else {
                        await _salaryRepository.SaveSalaries(salaryList);
                        serviceResponse.Success = true;
                        serviceResponse.Message = "Guardado correctamente.";                        
                    }
                }
            }
            catch (Exception e)
            {
                serviceResponse.Message = "Ocurrió un error interno.";
            }

            return serviceResponse;
        }
    }
}
