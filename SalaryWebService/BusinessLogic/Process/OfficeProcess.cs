﻿using AutoMapper;
using BusinessLogic.DTOs;
using BusinessLogic.Interfaces;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilities.Entity;

namespace BusinessLogic.Process
{
    public class OfficeProcess : IOffice
    {
        private readonly IOfficeRepository _officeRepository;
        private readonly IMapper _mapper;

        public OfficeProcess(IOfficeRepository officeRepository, IMapper mapper)
        {
            _officeRepository = officeRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResponse<BaseEntityDTO>> GetAll()
        {
            ServiceResponse<BaseEntityDTO> serviceResponse = new ServiceResponse<BaseEntityDTO>();

            try
            {
                var officeList = await _officeRepository.GetAll();
                serviceResponse.DataList = _mapper.Map<IList<BaseEntityDTO>>(officeList);
                serviceResponse.Success = true;
            }
            catch (Exception)
            {

            }

            return serviceResponse;
        }
    }
}
