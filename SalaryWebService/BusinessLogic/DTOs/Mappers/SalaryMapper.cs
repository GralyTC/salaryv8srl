﻿using AutoMapper;
using Entity;

namespace BusinessLogic.DTOs.Mapers
{
    public class SalaryMapper : Profile
    {
        public SalaryMapper() {
            CreateMap<Salary, EmployeeReportDTO>()
                .ForMember(d => d.PositionName, opt => opt.MapFrom(src => src.Position.PositionName))
                .ForMember(d => d.DivisionName, opt => opt.MapFrom(src => src.Division.DivisionName))
                .ForMember(d => d.EmployeeFullName, opt => opt.MapFrom(src => $"{src.EmployeeName} {src.EmployeeSurname}"));

            CreateMap<Salary, SalaryDTO>()
                .ForMember(d => d.PositionName, opt => opt.MapFrom(src => src.Position.PositionName))
                .ForMember(d => d.OfficeName, opt => opt.MapFrom(src => src.Office.OfficeName))
                .ForMember(d => d.DivisionName, opt => opt.MapFrom(src => src.Division.DivisionName))
                .ForMember(d => d.YearMonth, opt => opt.MapFrom(src => $"{src.Year}{MonthTwoCharacters(src.Month)}"))
                .ReverseMap()
                .ForPath(s => s.Position.PositionName, opt => opt.Ignore())
                .ForPath(s => s.Office.OfficeName, opt => opt.Ignore())
                .ForPath(s => s.Division.DivisionName, opt => opt.Ignore());

            CreateMap<Office, BaseEntityDTO>().ForMember(d => d.Id, opt => opt.MapFrom(src => src.OfficeId))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.OfficeName));
            CreateMap<Position, BaseEntityDTO>().ForMember(d => d.Id, opt => opt.MapFrom(src => src.PositionId))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.PositionName));
            CreateMap<Division, BaseEntityDTO>().ForMember(d => d.Id, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.DivisionName));

            CreateMap<SalaryDTO, EmployeeReportDTO>()
                .ForMember(d => d.EmployeeFullName, opt => opt.MapFrom(src => $"{src.EmployeeName} {src.EmployeeSurname}"));
        }

        public string MonthTwoCharacters(int month)
        {
            return month < 10 ? "0" + month : month.ToString();
        }
    }
}
