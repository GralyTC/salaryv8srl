﻿using System;

namespace BusinessLogic.DTOs
{
    public class EmployeeReportDTO
    {
        public string EmployeeCode { get; set; }
        public string EmployeeFullName { get; set; }
        public string DivisionName { get; set; }
        public string PositionName { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal TotalSalary { get; set; }
        public byte OfficeId { get; set; }
        public byte PositionId { get; set; }
        public int Grade { get; set; }
    }
}
