﻿using System;

namespace BusinessLogic.DTOs
{
    public class SalaryDTO
    {
        public int SalaryId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public byte OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSurname { get; set; }
        public byte DivisionId { get; set; }
        public string DivisionName { get; set; }
        public byte PositionId { get; set; }
        public string PositionName { get; set; }
        public int Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commission { get; set; }
        public decimal Contributions { get; set; }
        public decimal TotalSalary { get; set; }
        public int YearMonth { get; set; }
    }
}
