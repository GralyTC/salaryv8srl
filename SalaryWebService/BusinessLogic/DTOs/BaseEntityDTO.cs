﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.DTOs
{
    public class BaseEntityDTO
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
