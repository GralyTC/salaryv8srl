﻿using System.Threading.Tasks;
using Utilities.Entity;

namespace BusinessLogic.Interfaces
{
    public interface IBase<T>
    {
        Task<ServiceResponse<T>> GetAll();
    }
}
