﻿using BusinessLogic.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Entity;

namespace BusinessLogic.Interfaces
{
    public interface ISalary : IBase<EmployeeReportDTO>
    {
        Task<ServiceResponse<EmployeeReportDTO>> GetEmployeeSalaryByParamaters(byte gradeId, byte positionId, byte officeId);
        Task<ServiceResponse<EmployeeReportDTO>> GetSalariesByEmployeeCode(string employeeCode);
        Task<ServiceResponse<bool>> SaveSalaries(IList<SalaryDTO> salaryListDto);
    }
}
